import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


interface MoviePosition {
  episode: number;
  machete: number;
}

interface MovieDetails {
  Actors: string;
  Title: string;
}

interface Movie {
  imdbId: string;
  position: MoviePosition;
  movieDetails: MovieDetails;
  cover: string;
}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'StarWarsUI';
  baseUrl = 'http://localhost:8080/';
  movies: Movie[];
  userName = '';
  savingResponseMessage = '';

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
    this.setMovies(false).subscribe((res: Movie[]) => {
      this.movies = res;
    });
  }

  sort(obj: any) {
    this.movies.sort( (m1, m2) => {
      if (obj.target.checked) {
        return m1.position.machete - m2.position.machete;
      } else {
        return m1.position.episode - m2.position.episode;
      }
    });
  }

  saveAsFavorite(movieId: string) {
    if (this.userName === '') {
      this.savingResponseMessage = 'No user selected';
    } else {
      const url = this.baseUrl.concat('api/users/favoriteMovie');
      console.log('Saving favorite movie : ' + movieId + ' for user : ' + this.userName);

      this.httpClient.post(url, {
          userid: this.userName,
          movieid: movieId
      }, {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
        })
      }).subscribe((res: any) => {
        console.log('Response from the server :' + res.response);
        this.savingResponseMessage = res.response;
      });
    }
  }

  setMovies(machete: boolean) {
    const url = this.baseUrl.concat('api/movies/');
    console.log('Sending get request to : ' + url);
    return this.httpClient.get(url, {
      params: {
        order: machete ? 'MACHETE' : 'CHRONOLOGICALLY'
      }
    });
  }
}
